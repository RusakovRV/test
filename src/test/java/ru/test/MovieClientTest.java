package ru.test;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

import ru.test.exception.APIClientException;
import ru.test.exception.GenreNotFoundException;
import ru.test.exception.ResultCalculationException;


public class MovieClientTest {

    private MovieClient movieClient;

    @Before
    public void init() {
        movieClient = new MovieClient();
    }


    @Test
    public void testStartAndStopCalculations() throws APIClientException, ResultCalculationException, GenreNotFoundException {
        assertThat(movieClient.runAverageCalculation(18)).isTrue();
        assertThat(movieClient.cancelCalculation()).isTrue();
        assertThat(movieClient.isCanceled()).isTrue();
        assertThat(movieClient.isDone()).isFalse();
        assertThat(movieClient.getProgress()).isNull();
    }

    @Test
    public void testStartCalculationsWithNonExistingGenreId() throws APIClientException, ResultCalculationException, GenreNotFoundException {
        Throwable thrown = catchThrowable(() -> {
            movieClient.runAverageCalculation(0);
        });
        assertThat(thrown).isInstanceOf(GenreNotFoundException.class);
        assertThat(thrown.getMessage()).isNotBlank();
    }

    @Test
    public void testInvalidUrl() {
        Throwable thrown = catchThrowable(() -> {
            String wrongUrl = "https://easy.test-assignment.net";
            String genrePath = "/3/discover/movie";
            String moviePath = "/3/discover/movie";
            String apiKey = "72b56103e43843412a992a8d64bf96e9";
            MovieClient movieClient = new MovieClient(wrongUrl, genrePath, moviePath, apiKey);
            movieClient.runAverageCalculation(18);
        });
        assertThat(thrown).isInstanceOf(APIClientException.class);
        assertThat(thrown.getMessage()).isNotBlank();
    }

}
