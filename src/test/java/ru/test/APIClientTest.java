package ru.test;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;


import ru.test.exception.APIClientException;
import ru.test.model.GenreDTO;
import ru.test.model.MovieResponseDTO;

import java.util.List;


public class APIClientTest {

    private APIClient apiClient;

    @Before
    public void init() {
        apiClient = new APIClient();
    }

    @Test
    public void testGetMovies() throws APIClientException {
        MovieResponseDTO m = apiClient.getMovies();
        assertThat(m.getResults()).isNotEmpty();
        assertThat(m.getTotalPages()).isGreaterThan(0);
        assertThat(m.getTotalResults()).isGreaterThan(0);
        assertThat(m.getPage()).isEqualTo(1);
        assertThat(m.getResults().get(0).getId()).isGreaterThan(0);
    }

    @Test
    public void testGetMoviesByPage() throws APIClientException {
        MovieResponseDTO m = apiClient.getMovies(1);
        assertThat(m.getResults()).isNotEmpty();
        assertThat(m.getTotalPages()).isGreaterThan(0);
        assertThat(m.getTotalResults()).isGreaterThan(0);
        assertThat(m.getPage()).isEqualTo(1);
        assertThat(m.getResults().get(0).getId()).isGreaterThan(0);
    }

    @Test
    public void testGetGenres() throws APIClientException {
        List<GenreDTO> g = apiClient.getGenres();
        assertThat(g).isNotEmpty();
    }

    @Test
    public void testInvalidUrl() {
        Throwable thrown = catchThrowable(() -> {
            APIClient brokenApiClient = new APIClient();
            brokenApiClient.setUrl("https://easy.test-assignment-a.loyaltyplant1.net");
            brokenApiClient.getMovies();
        });
        assertThat(thrown).isInstanceOf(APIClientException.class);
        assertThat(thrown.getMessage()).isNotBlank();
    }

    @Test
    public void testInvalidGenrePath() {
        Throwable thrown = catchThrowable(() -> {
            APIClient brokenApiClient = new APIClient();
            brokenApiClient.setGenrePath("/1/genre/movie/list");
            brokenApiClient.getGenres();
        });
        assertThat(thrown).isInstanceOf(APIClientException.class);
        assertThat(thrown.getMessage()).isNotBlank();
    }

    @Test
    public void testInvalidMoviePath() {
        Throwable thrown = catchThrowable(() -> {
            APIClient brokenApiClient = new APIClient();
            brokenApiClient.setMoviePath("/1/discover/movie");
            brokenApiClient.getMovies();
        });
        assertThat(thrown).isInstanceOf(APIClientException.class);
        assertThat(thrown.getMessage()).isNotBlank();
    }

}
