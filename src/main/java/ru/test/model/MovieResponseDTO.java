package ru.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MovieResponseDTO {
    private int page;
    @JsonProperty("total_results")
    private int totalResults;
    @JsonProperty("total_pages")
    private int totalPages;
    private List<MovieDTO> results;

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<MovieDTO> getResults() {
        return results;
    }

    public static class MovieDTO {

        private int id;
        private int vote_count;
        private boolean video;
        @JsonProperty("vote_average")
        private float voteAverage;
        private String title;
        private float popularity;
        @JsonProperty("original_language")
        private String originalLanguage;
        @JsonProperty("original_title")
        private String originalTitle;
        @JsonProperty("genre_ids")
        private List<Integer> genreIds;
        private boolean adult;
        private String overview;
        @JsonProperty("release_date")
        private List<Integer> releaseDate;

        public int getId() {
            return id;
        }

        public int getVote_count() {
            return vote_count;
        }

        public boolean isVideo() {
            return video;
        }

        public float getVoteAverage() {
            return voteAverage;
        }

        public String getTitle() {
            return title;
        }

        public float getPopularity() {
            return popularity;
        }

        public String getOriginalLanguage() {
            return originalLanguage;
        }

        public String getOriginalTitle() {
            return originalTitle;
        }

        public List<Integer> getGenreIds() {
            return genreIds;
        }

        public boolean isAdult() {
            return adult;
        }

        public String getOverview() {
            return overview;
        }

        public List<Integer> getReleaseDate() {
            return releaseDate;
        }

        @Override
        public String toString() {
            return "MovieDTO{" +
                    "id=" + id +
                    ", vote_count=" + vote_count +
                    ", video=" + video +
                    ", voteAverage=" + voteAverage +
                    ", title=" + title +
                    ", popularity=" + popularity +
                    ", originalLanguage=" + originalLanguage +
                    ", originalTitle=" + originalTitle +
                    ", genreIds=" + genreIds +
                    ", adult=" + adult +
                    ", overview=" + overview +
                    ", releaseDate=" + releaseDate +
                    '}';
        }

    }
}
