package ru.test;

import ru.test.exception.APIClientException;
import ru.test.exception.GenreNotFoundException;
import ru.test.exception.ResultCalculationException;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws InterruptedException, GenreNotFoundException, APIClientException, ResultCalculationException {
        MovieClient m = new MovieClient("https://easy.test-assignment-a.loyaltyplant.net", "/3/genre/movie/list", "/3/discover/movie", "72b56103e43843412a992a8d64bf96e9");
        m.runAverageCalculation(18);
        System.out.println("progress: " + m.getProgress());
        Thread.sleep(5000);
        System.out.println("progress: " + m.getProgress());
        Thread.sleep(5000);
        System.out.println("progress: " + m.getProgress() + "%");
        System.out.println("cancel called " +  m.cancelCalculation());
        System.out.println("progress: " + m.getProgress());
        System.out.println("isDone: " + m.isDone());
        System.out.println("isCanceled: " + m.isCanceled());
        m.runAverageCalculation(35);
        Thread.sleep(5000);
        System.out.println("progress: "  + m.getProgress() + "%");
        Thread.sleep(5000);
        System.out.println("progress: "  + m.getProgress() + "%");
        Thread.sleep(5000);
        System.out.println("progress: "  + m.getProgress() + "%");
        System.out.println("WaitForResult with timeout: " + m.waitForResult(2000l, TimeUnit.MICROSECONDS));
        System.out.println("progress: "  + m.getProgress() + "%");
        System.out.println("WaitForResult: " + m.waitForResult());
    }

}
