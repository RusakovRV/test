package ru.test.exception;

public class APIClientException extends Exception {
    public APIClientException() {
        super();
    }

    public APIClientException(String message) {
        super(message);
    }

    public APIClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
