package ru.test.exception;

public class ResultCalculationException extends Exception {

    public ResultCalculationException() {
        super();
    }

    public ResultCalculationException(String message) {
        super(message);
    }

    public ResultCalculationException(String message, Throwable cause) {
        super(message, cause);
    }
}



