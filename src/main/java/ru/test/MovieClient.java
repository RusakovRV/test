package ru.test;

import ru.test.exception.APIClientException;
import ru.test.exception.GenreNotFoundException;
import ru.test.exception.ResultCalculationException;
import ru.test.model.MovieResponseDTO;

import java.util.List;
import java.util.concurrent.*;


/**
 * Класс для запуска подсчёта средней оценки для заданного жанра
 */
public class MovieClient {
    APIClient apiClient;
    volatile private int currentPage;
    volatile private int pageCount;
    Future<Double> future;

    public MovieClient(String url, String genrePath, String moviePath, String apiKey) {
        apiClient = new APIClient(url, genrePath, moviePath, apiKey);
        future = null;
    }

    public MovieClient() {
        apiClient = new APIClient();
        future = null;
    }

    /**
     * Метод подсчёта средней оценки
     *
     * @param genreId Идентификатор жанра, по которому производится подсчёт
     * @return Средняя оценка. Если фильмов с указанным жанром нет или если ни у одного нет оценки, вернёт 0.0
     */
    private Double calculateAverageVote(final int genreId) throws APIClientException {
        currentPage = 0;
        pageCount = apiClient.getMovies().getTotalPages();
        double sum = 0;
        long count = 0;
        for (int i = 1; i < pageCount; i++) {
            if (Thread.currentThread().isInterrupted()) {
                return null;
            }
            List<MovieResponseDTO.MovieDTO> response = apiClient.getMovies(i).getResults();
            if (response == null) {
                response = apiClient.getMovies(i).getResults();
            }
            if (response != null)
                for (MovieResponseDTO.MovieDTO m : response) {
                    if (m.getGenreIds().contains(genreId) && m.getVoteAverage() >= 1) {
                        count++;
                        sum += m.getVoteAverage();
                    }
                }
            currentPage++;
        }
        if (count != 0) {
            return sum / count;
        }
        return 0.0;
    }

    /**
     * Запуск подсчёта оценки в отдельном потоке
     *
     * @param genreId Идентификатор жанра, по которому производится подсчёт
     * @return Средняя оценка
     */
    public boolean runAverageCalculation(int genreId) throws GenreNotFoundException, APIClientException {
        if (future == null || future.isCancelled()) {
            if (apiClient.getGenres().stream().filter(g -> g.getId() == genreId).count() == 0) {
                throw new GenreNotFoundException("Genre with id: " + genreId + " was not found");
            }
            ExecutorService executor = Executors.newSingleThreadExecutor();
            future = executor.submit(() -> calculateAverageVote(genreId));
            executor.shutdown();
            return true;
        }
        return false;
    }

    /**
     * Получить результат, если он уже готов
     *
     * @return Средняя оценка, если подсчёт не завершён, не начат или отменён, то вернёт null
     */
    public Double getResultIfDone() throws ResultCalculationException {
        if (future != null && !future.isCancelled() && future.isDone()) {
            try {
                Double result;
                result = future.get();
                future = null;
                return result;
            } catch (InterruptedException | ExecutionException e) {
                throw new ResultCalculationException("Error while waiting for result", e);
            }
        }
        return null;
    }

    /**
     * Ожидать результат определённое время
     *
     * @param timeout  Время ожидания
     * @param timeUnit Единицы измерения времени ожидания
     * @return Средняя оценка, если подсчёт не начат или отменён или истекло время ожидания, то вернёт null
     */
    public Double waitForResult(Long timeout, TimeUnit timeUnit) throws ResultCalculationException {
        if (future != null && !future.isCancelled()) {
            try {
                Double result;
                if (timeout != null && timeUnit != null) {
                    result = future.get(timeout, timeUnit);
                    future = null;
                    return result;
                } else {
                    result = future.get();
                    future = null;
                    return result;
                }
            } catch (InterruptedException | ExecutionException e) {
                throw new ResultCalculationException("Error while waiting for result", e);
            } catch (TimeoutException e) {
            }
        }
        return null;
    }

    /**
     * Ожидать результат определённое время
     *
     * @return Средняя оценка, если подсчёт не начат или отменён, то вернёт null
     */
    public Double waitForResult() throws ResultCalculationException {
        return waitForResult(null, null);
    }

    /**
     * Получение текущего прогресса выполнения подсчёта
     *
     * @return Процент выполнения, если подсчёт не начался или отменён, то вернёт null
     */
    public Integer getProgress() {
        if (future != null && !future.isCancelled() && pageCount != 0) {
            return (int) ((double) currentPage / (double) pageCount * 100);
        }
        return null;
    }


    /**
     * Статус завершения подсчёта
     *
     * @return Если подсчёт завершён, вернёт true, иначе false
     */
    public boolean isDone() {
        if (future != null && !future.isCancelled())
            return future.isDone();
        return false;
    }


    /**
     * Отмена подсчёта
     *
     * @return Если подсчёт удалось прервать, вернёт true, иначе false
     */
    public boolean cancelCalculation() {
        if (future != null)
            return future.cancel(true);
        return false;
    }

    /**
     * Отменён ли подсчёт
     *
     * @return Если подсчёт отменён, вернёт true
     */
    public boolean isCanceled() {
        if (future != null)
            return future.isCancelled();
        return false;
    }

}
