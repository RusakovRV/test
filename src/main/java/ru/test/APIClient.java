package ru.test;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import ru.test.exception.APIClientException;
import ru.test.model.GenreDTO;
import ru.test.model.MovieResponseDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class APIClient {
    private String url;
    private String genrePath;
    private String moviePath;
    private String apiKey;
    ObjectMapper mapper;
    JsonFactory factory;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setGenrePath(String genrePath) {
        this.genrePath = genrePath;
    }

    public void setMoviePath(String moviePath) {
        this.moviePath = moviePath;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public APIClient(String url, String genrePath, String moviePath, String apiKey) {
        this.url = url;
        this.genrePath = genrePath;
        this.moviePath = moviePath;
        this.apiKey = apiKey;
        mapper = new ObjectMapper();
        factory = mapper.getFactory();
    }

    public APIClient() {
        this.url = "https://easy.test-assignment-a.loyaltyplant.net";
        this.genrePath = "/3/genre/movie/list";
        this.moviePath = "/3/discover/movie";
        this.apiKey = "72b56103e43843412a992a8d64bf96e9";
        mapper = new ObjectMapper();
        factory = mapper.getFactory();
    }

    public List<GenreDTO> getGenres() throws APIClientException {
        String responseStr = getStringResponse(genrePath);
        if (responseStr != null) {
            try {
                JsonParser parser = factory.createParser(responseStr);
                JsonNode actualObj = mapper.readTree(parser);
                if (actualObj.get("genres") != null) {
                    return Arrays.asList(mapper.treeToValue(actualObj.get("genres"), GenreDTO[].class));
                }
            } catch (IOException e) {
                throw new APIClientException("Error while reading genres", e);
            }
        }
        throw new APIClientException("Cannot get list of genres");
    }

    public MovieResponseDTO getMovies(Integer page) throws APIClientException {
        String responseStr = getStringResponse(moviePath, page);
        if (responseStr != null) {
            try {
                return mapper.readValue(responseStr, MovieResponseDTO.class);
            } catch (IOException e) {
                throw new APIClientException("Error while parsing movies list", e);
            }
        }
        throw new APIClientException("Cannot get list of movies from response");
    }

    public MovieResponseDTO getMovies() throws APIClientException {
        return getMovies(null);
    }

    private String getStringResponse(String path, Integer page) throws APIClientException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            URIBuilder builder = new URIBuilder(url);
            builder.setPath(path);
            builder.setParameter("api_key", apiKey);
            if (page != null)
                builder.setParameter("page", page.toString());
            HttpGet request = new HttpGet(builder.build());
            CloseableHttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new APIClientException("Error getting http response from host: " + response.getStatusLine());
            }
            if (response != null && response.getEntity() != null) {
                return EntityUtils.toString(response.getEntity());
            }
        } catch (URISyntaxException e) {
            throw new APIClientException("Error with url", e);
        } catch (IOException e) {
            throw new APIClientException("Error getting http response from host", e);
        }
        return null;
    }

    private String getStringResponse(String path) throws APIClientException {
        return getStringResponse(path, null);
    }

}
